const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/index.js', 'dist/scripts')
    .postCss('resources/styles/index.css', 'dist/styles', [
        require('tailwindcss'),
        require('autoprefixer'),
        ...(mix.inProduction() ? [require('cssnano')({
            preset: ['default', {
                discardComments: {
                    removeAll: true,
                },
            }]
        })] : []),
    ])
    .setPublicPath('dist')
    // HACK to fix problems with url in css files
    .setResourceRoot('../../dist/')
    .disableNotifications()

if (mix.inProduction()) {
    mix.version();
}