<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:300,400,700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class('font-sans'); ?>>

    <div id="page" class="site flex flex-col min-h-screen">
        <?php blade_view('header'); ?>
        <div id="content" class="site-content flex-grow">