<?php

namespace App;

use App\Base\Support\Str;

class Site
{
    public static function logoUrl()
    {
        if (!has_custom_logo()) {
            return '#';
        }
        $custom_logo_id = get_theme_mod('custom_logo');
        $image = wp_get_attachment_image_src($custom_logo_id, 'full');
        return $image[0];
    }
}
