<?php

/**
 * Enter your helpers here
 */

use App\Theme\Language;

if (!function_exists('logo_url')) {
    function logo_url()
    {
        return App\Site::logoUrl();
    }
}

if (!function_exists('get_asset_handle')) {
    /**
     * @param string $key
     * @param string $type
     * @return string
     */
    function get_asset_handle($key, $type = 'script')
    {
        return App\Theme\Theme::getInstance()->getAssetHandle($key, $type);
    }
}

if (!function_exists('get_asset_url')) {
    function get_asset_url($filePath)
    {
        return get_template_directory_uri() . "/build/{$filePath}";
    }
}

if (!function_exists('app_lang')) {
    /**
     * Get translation string
     * @param string $path
     * @param string $default
     */
    function app_lang($path, $default = null)
    {
        return Language::getInstance()->get($path, $default);
    }
}

/**
 *
 * @param string $view
 * @param array $data
 * @param array $mergeData
 * @return void
 * @throws InvalidArgumentException
 */
function blade_view(string $view, array $data = [], array $mergeData = [])
{
    echo blade_view_html($view, $data, $mergeData);
}

/**
 * @param string $view
 * @param array $data
 * @param array $mergeData
 * @return string
 * @throws InvalidArgumentException
 */
function blade_view_html(string $view, array $data = [], array $mergeData = [])
{
    return \App\Blade\Application::init()->blade()->render($view, $data, $mergeData);
}
