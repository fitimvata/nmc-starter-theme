<?php

namespace App\Theme;

use App\Base\Singleton;
use App\Base\Support\Arr;

class Language extends Singleton
{
    public $dir = 'languages';
    public $data = [];

    protected function __construct()
    {
        $this->data = $this->getFile('strings');
        add_action('wp_head', [$this, 'loadStrings']);
    }

    public function loadStrings()
    {
        $id = app_prefix() . "-translations";
        $data =  wp_json_encode($this->data);
        echo <<<HTML
<script type="text/javascript" id="{$id}">
    window.TRANSLATIONS = {$data};
</script>
HTML;
        ?>
       
        <?php
    }

    public function getFile($filename)
    {
        $data = require get_template_directory() . DIRECTORY_SEPARATOR . $this->dir . DIRECTORY_SEPARATOR . $filename . '.php';  // phpcs:ignore
        return apply_filters(app_prefix() . '_translations', $data ?? []);
    }

    public function get($path, $default = null)
    {
        return Arr::get($this->data, $path, $default);
    }
}
