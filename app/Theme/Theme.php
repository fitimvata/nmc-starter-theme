<?php

namespace App\Theme;

use App\Base\Singleton;

class Theme extends Singleton
{

    /**
     * @var string
     */
    public $version;

    /**
     * @var string
     */
    public $resource_dir;

    /**
     * @var string
     */
    public $prefix;

    /**
     * @var array asset mix manifest
     */
    public $manifest = [];

    /**
     * @var array
     */
    public $stylesheet_files = [];

    /**
     * @var array
     */
    public $register_override_scripts = [];

    /**
     * @var array
     */
    public $scripts_files = [];

    /**
     * @var array
     */
    public $scripts_urls = [];

    protected function __construct()
    {
        $this->setup();
        $this->serVersion();
        $this->setMixFile();
        add_action('init', [$this, 'registerOptionPage']);
        add_action('wp_head', [$this, 'jsVars']);
        add_action('widgets_init', [$this, 'sidebar']);
        add_action('after_setup_theme', [$this, 'setUpTheme']);
        add_action('wp_enqueue_scripts', [$this, 'registerAssets'], 1);
        add_action('wp_enqueue_scripts', [$this, 'loadAssets'], 10);
    }

    private function setup()
    {
        $this->resource_dir = config('app.assets.build_dir');
        $this->prefix = config('app.assets.prefix', app_prefix());
        $this->stylesheet_files = config('app.assets.stylesheet_files');
        $this->scripts_files = config('app.assets.scripts_files');
        $this->scripts_urls = config('app.assets.scripts_urls');
        $this->register_override_scripts = config('app.assets.override_scripts');
    }

    /**
     * Read version from composer.json
     * @return void
     */
    private function serVersion()
    {
        $content = file_get_contents(get_template_directory() . DIRECTORY_SEPARATOR . 'package.json');
        $content = json_decode($content, true);
        $this->version = $content['version'] ?? '0.0.0';
    }

    /**
     * Read mix-manifest for compiled assets
     * @return void
     */
    private function setMixFile()
    {
        $mixFilePath = get_template_directory() . DIRECTORY_SEPARATOR . $this->resource_dir . DIRECTORY_SEPARATOR . 'mix-manifest.json'; // phpcs:ignore
        if (!file_exists($mixFilePath)) {
            return;
        }
        $content = file_get_contents($mixFilePath);
        $content = json_decode($content, true);
        $this->manifest = $content;
    }

    /**
     * setup theme
     * @return void
     */
    public function setUpTheme()
    {
        load_theme_textdomain(texdomain(), get_template_directory() . DIRECTORY_SEPARATOR . 'languages');
        $this->registerNavMenus();
        $this->registerSupports();
    }

    /**
     * @see https://codex.wordpress.org/Function_Reference/register_nav_menu
     */
    public function registerNavMenus()
    {
        register_nav_menus(config('app.nav_menus', []));
    }

    public function registerOptionPage()
    {

        if (function_exists('acf_add_options_page')) {
            //@php-ignore
            \acf_add_options_page(config('app.admin_option_page', []));
        }
    }

    /**
     * @see https://developer.wordpress.org/reference/functions/add_theme_support/
     */
    public function registerSupports()
    {
        foreach (config('app.supports', []) as $feature => $args) {
            if (!$args) {
                add_theme_support($feature);
            } else {
                add_theme_support($feature, $args);
            }
        }
    }

    /**
     * @see https://codex.wordpress.org/Function_Reference/register_sidebar
     */
    public function sidebar()
    {
        $sidebars = apply_filters(app_prefix() . '_register_sidebars', config('app.sidebars'));

        foreach ($sidebars as $id => $sidebar) {
            register_sidebar([
                'name' => $sidebar['name'],
                'id' => $id,
                'description' => $sidebar['description'],
                'before_widget' => $sidebar['before_widget'] ?? '<section id="%1$s" class="widget %2$s">',
                'after_widget' => $sidebar['after_widget'] ?? '</section>',
            ]);
        }
    }

    /**
     * @see https://codex.wordpress.org/Function_Reference/wp_localize_script
     */
    public function jsVars()
    {

        $INITIAL_DATA = apply_filters(
            app_prefix() . '_js_initial_data',
            [
                'ajax_url' => admin_url('admin-ajax.php'),
            ]
        );
        $string_data = wp_json_encode($INITIAL_DATA);
        echo <<<HTML
<script type="text/javascript">
    window.INITIAL_DATA = {$string_data};
</script>
HTML;
    }

    public function registerAssets()
    {
        $uri = get_template_directory_uri() . '/' . $this->resource_dir;
        $path = get_template_directory() . DIRECTORY_SEPARATOR . $this->resource_dir;
        foreach ($this->register_override_scripts as $file => $options) {
            $mix_file = $this->manifest[$file] ?? $file;

            if (file_exists($path . $file)) {
                wp_deregister_script($options['key']);

                wp_register_script(
                    $options['key'], // handle
                    $uri . $mix_file, // src
                    $options['deps'] ?? [], // deps
                    $this->version, // version
                    $options['in_footer'] // in_footer
                );

                if (isset($options['include']) && $options['include']) {
                    wp_enqueue_script($options['key']);
                }
            }
        }
    }

    public function loadAssets()
    {
        $this->loadStyleSheets();
        $this->loadScripts();
    }

    protected function loadStyleSheets()
    {
        $uri = get_template_directory_uri() . '/' . $this->resource_dir;
        $path = get_template_directory() . DIRECTORY_SEPARATOR . $this->resource_dir;

        foreach ($this->stylesheet_files as $key => $file) {
            $mix_file = $this->manifest[$file] ?? $file;
            $fullPath = $path . $file;
            if (file_exists($fullPath)) {
                $fileSize = filesize($fullPath) >> 10;
                if ($fileSize <= 100) {
                    wp_register_style(
                        $this->getAssetHandle($key, 'style'),
                        false,
                        [], // deps
                        $this->version, // version
                        'all' // media
                    );
                    wp_enqueue_style($this->getAssetHandle($key, 'style'));
                    wp_add_inline_style(
                        $this->getAssetHandle($key, 'style'),
                        file_get_contents($fullPath)
                    );
                } else {
                    wp_enqueue_style(
                        $this->getAssetHandle($key, 'style'), // handle
                        $uri . $mix_file, // src
                        [], // deps
                        $this->version, // version
                        'all' // media
                    );
                }
            }
        }
    }

    protected function loadScripts()
    {

        foreach ($this->scripts_urls as $url => $options) {
            if (is_array($options['params'])) {
                $url = $url . '?' . http_build_query($options['params']);
            }
            $handle = $this->getAssetHandle($options['key']);
            wp_register_script(
                $handle, // handle
                $url, // src
                $options['deps'] ?? [], // deps
                null, // version
                $options['in_footer'] // in_footer
            );

            if (isset($options['include']) && $options['include']) {
                wp_enqueue_script($handle);
            }
        }

        $uri = get_template_directory_uri() . '/' . $this->resource_dir;
        $path = get_template_directory() . DIRECTORY_SEPARATOR . $this->resource_dir;

        foreach ($this->scripts_files as $file => $options) {
            $mix_file = $this->manifest[$file] ?? $file;

            if (file_exists($path . $file)) {
                $handle = $this->getAssetHandle($options['key']);
                wp_register_script(
                    $handle, // handle
                    $uri . $mix_file, // src
                    $options['deps'] ?? [], // deps
                    $this->version, // version
                    $options['in_footer'] // in_footer
                );

                if (isset($options['include']) && $options['include']) {
                    wp_enqueue_script($handle);
                }
            }
        }
    }

    /**
     * @param string $key
     * @param string $type
     * @return string
     */
    public function getAssetHandle($key, $type = 'script')
    {
        if ($type === 'script') {
            return $this->prefix . '-scripts-url-' . $key;
        }

        if ($type === 'style') {
            return $this->prefix . '-style-' . $key;
        }

        return '';
    }
}
