<?php

namespace App\Http;

use App\Base\Ajax;
use Symfony\Component\HttpFoundation\Request;

class DummyAjax extends Ajax
{
    public $protection = [Ajax::PUBLIC_LEVEL, Ajax::PRIVATE_LEVEL];
    public function handle(Request $request)
    {
        return json([
            'foo' => 'bar2'
        ])->status(200);
    }
}
