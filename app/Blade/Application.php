<?php

namespace App\Blade;

use App\Base\Singleton;
use Illuminate\Container\Container;
use Illuminate\Contracts\View\Factory;
use Jenssegers\Blade\Blade;

class Application extends Singleton
{
    /**
     *
     * @var Container
     */
    private $app;

    /**
     *
     * @var Blade
     */
    private $blade;

    protected function __construct()
    {
        $this->app = new FakeApplication(__APP_BASE_PATH);
        $this->blade = new Blade(__APP_BASE_PATH . DIRECTORY_SEPARATOR . 'resources/views', __APP_BASE_PATH . DIRECTORY_SEPARATOR . 'cache/views', $this->app);
        $this->app->bind(Factory::class, function () {
            return $this->blade;
        });
    }

    /**
     *
     * @return Blade 
     */
    public function blade()
    {
        return $this->blade;
    }
}
