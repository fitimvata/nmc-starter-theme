<?php

return [
    "prefix" => "nmc",

    "logs" => [
        /**
         * 
         * drivers: local
         * 
         */
        "driver" => "local",

        /**
         * 
         * name based on your project name
         * 
         */
        "logger_name" => "nmc",
    ],

    "supports" => [
        "title-tag" => null,
        "post-thumbnails" => null,
        "post-formats" => ['gallery', 'video'],
        "custom-header" => null,
        "custom-logo" => null,
        "html5" => [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption'
        ],
    ],

    "admin_option_page" => [
        'page_title' => __('Options', 'nmc'),
        'menu_title' => __('Options', 'nmc'),
        'menu_slug' => 'options',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-admin-generic',
    ],

    'nav_menus' => [
        'primary' => __('Primary Menu', 'nmc'),
    ],
    "sidebars" => [
        "sidebar" => [
            'name' => __('Sidebar', 'nmc'),
            'description' => __('Default sidebar', 'nmc'),
        ],
    ],

    "assets" => [
        "prefix" => "nmc",
        "build_dir" => "dist",
        "stylesheet_files" => [
            '/styles/index.css',
        ],
        "override_scripts" => [
        ],
        "scripts_files" => [
            '/js/index.js' => [
                'key' => 'app',
                'in_footer' => true,
                'deps' => [],
                'include'=> true,
            ],
        ],
        "scripts_urls" => [
            'https://maps.googleapis.com/maps/api/js' => [
                'key' => 'google-maps',
                'params' => [
                    'v' => '3.exp',
                    'key' => '',
                ],
                'in_footer' => true,
                'include'=> false,
            ],
        ],
    ],
];
