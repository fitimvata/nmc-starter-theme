
@props(['name'])
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:300,400,700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class('font-sans'); ?>  data-page-name="{{ $name ?? 'default' }}">

  <div id="page" class="site flex flex-col min-h-screen">
    <x-header />
    <div id="content" class="site-content flex-grow">
    {{ $slot }}
    </div>
    <x-footer />
  </div><!-- #page -->
  <?php wp_footer(); ?>
</body>
</html>