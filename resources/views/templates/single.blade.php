
<x-layouts.default name="about">
    <div class="container">
        @php
            the_post();   
        @endphp
        <h1 class="text-3xl font-bold">{{the_title()}}</h1>
        <div>
            {{  the_content() }}
        </div>
    </div>
</x-layouts.default>