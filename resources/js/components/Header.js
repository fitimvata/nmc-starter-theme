let menuContainer;
let menuToggle;

function init() {
  menuContainer = document.querySelector(".js-menu");
  menuToggle = document.querySelector(".js-menu-trigger");

  if (menuToggle && menuContainer) {
    menuToggle.addEventListener("click", toggleMenu);
  }
}

/**
 * @param {Event} e
 */
function toggleMenu(e) {
  const closeIcon = menuToggle.querySelector("[data-close-icon]");
  const menuIcon = menuToggle.querySelector("[data-menu-icon]");
  const active = menuContainer.getAttribute("data-active") === "true";
  const activeClasses = menuContainer
    .getAttribute("data-active-classes")
    .split(" ");
  const inActiveClasses = menuContainer
    .getAttribute("data-inactive-classes")
    .split(" ");
  if (active) {
    menuContainer.classList.add(...inActiveClasses);
    menuContainer.classList.remove(...activeClasses);
    if (closeIcon) {
      closeIcon.style.display = "none";
    }

    if (menuIcon) {
      menuIcon.style.display = "block";
    }

    menuContainer.removeAttribute("data-active");
  } else {
    menuContainer.classList.remove(...inActiveClasses);
    menuContainer.classList.add(...activeClasses);
    if (closeIcon) {
      closeIcon.style.display = "block";
    }

    if (menuIcon) {
      menuIcon.style.display = "none";
    }

    menuContainer.setAttribute("data-active", "true");
  }
}

export default { init };
