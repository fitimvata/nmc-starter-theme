/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import "regenerator-runtime/runtime";

import Common from "pages/Common";

const pages = {
  home: () => import("pages/Home"),
  about: () => import("pages/About"),
};

async function init() {
  Common.init();

  const { pageName } = document.body.dataset;
  if (!pageName) {
    return;
  }

  const pageModule = pages[pageName];

  if (!pageModule) {
    return;
  }
  const { default: page } = await pageModule();
  page.init();
}

init();

(async function () {
  
  const { wp_ajax } = await import("./wp")
  wp_ajax('dummy_ajax').get()
    .then(response => console.log(response))
    .catch(error => console.log(error))
})();