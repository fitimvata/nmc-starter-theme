import Ajax from './Ajax'
import Translate from './Translate'

/**
 * 
 * @param {string} action 
 */
function wp_ajax(action) {
  return Ajax.init(action)
}

export {
  Ajax,
  Translate,
  wp_ajax,
}