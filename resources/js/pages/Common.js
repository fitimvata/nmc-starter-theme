import Header from "components/Header";

function init() {
  Header.init();
}

export default { init };
