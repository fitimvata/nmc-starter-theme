const {
  fontFamily,
} = require("tailwindcss/defaultTheme")

module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
  },
  experimental: {
    applyComplexClasses: true,
    uniformColorPalette: true,
    extendedSpacingScale: true,
    defaultLineHeights: true,
    extendedFontSizeScale: true,
  },
  purge: {
    content: [
      'resources/**/*.js',
      'resources/**/*.php',
      'resources/**/*.ts',
      'app/**/*.php',
      './*.php',
    ],

    // These options are passed through directly to PurgeCSS
    options: {
      // whitelist: ['bg-red-500', 'px-4'],
      whitelistPatterns: [/post-content/, /flickity/, /mfp-/, /widgettitle/, /pagination/],
      whitelistPatternsChildren: [/post-content/, /woocommerce/, /widget/, /select2/, /widgettitle/, /pagination/],
    }
  },
  theme: {
    container: {
      center: true,
      padding: {
        default: "1.25rem",
      },
    },
    fontFamily: {
      ...fontFamily,
      sans: ['"IBM Plex Sans"', ...fontFamily.sans],
    },
    extend: {},
  },
  variants: {},
  plugins: [],
}
